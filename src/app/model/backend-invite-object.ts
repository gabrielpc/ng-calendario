

export interface BackendInviteObject {
    event: string;  
    sender: string;
    invitedUsers: string[];    
  }