export class InvitedUserField {
    username: string;
    notFound: boolean;
    alreadyInvited: boolean;
    selfInvited: boolean;
  
    constructor() {
      this.notFound = false;
      this.alreadyInvited = false;
      this.selfInvited = false;
    }
  }