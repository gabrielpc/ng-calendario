import { CalendarEvent } from 'angular-calendar';

export interface Invitation {
    eventID: number;
    usersToInvite: string[];
}