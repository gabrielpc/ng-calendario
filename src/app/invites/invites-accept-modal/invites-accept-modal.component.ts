import { Subscription } from 'rxjs/Subscription';
import { AuthService } from './../../auth/auth.service';
import { Observable } from 'rxjs/Observable';

import { DateComparison } from './../../utils/dateComparison';
import { InvitesService } from './../invites.service';
import { Invitation } from '../../model/invitation';
import { CalendarEvent } from 'angular-calendar';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import { Component, OnInit, Input, Output, ViewChild, EventEmitter, OnDestroy } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal';
import { CalendarioService } from '../../calendario/calendario.service';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { setInterval } from 'timers';

@Component({
  selector: 'invites-accept-modal',
  templateUrl: './invites-accept-modal.html',
  styleUrls: ['./invites-accept-modal.css']
})
export class InvitesAcceptModalComponent implements OnInit, OnDestroy {

  @ViewChild('modalContent') modalContent: TemplateRef<any>;  
  @Input() allEvents: CalendarEvent[] = [];
  @Output() refreshCalendarTrigger: EventEmitter<any> = new EventEmitter<any>();

  public modalRef: NgbModalRef;
  invitedEvent: CalendarEvent;
  inviteOwnerName: String;
  modalIsOpened: boolean = false;

  


  errorMessage: string;
  showErrorDiv: boolean = false;

  constructor(private modal: NgbModal,
  private calendarioService: CalendarioService,
  private invitesService: InvitesService,
  private authService: AuthService) { }

  private subscription: Subscription;


  ngOnInit() {    
    this.subscription = this.invitesService.openInvitesAcceptModalTrigger.subscribe((invitedEvent) => {       
      this.openModal(invitedEvent);        
    });  
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public openModal(invitedEvent: CalendarEvent): void {   
    
    if (!this.modalIsOpened) {
      this.showErrorDiv = false;   
      this.invitedEvent = invitedEvent;

      this.invitesService.getOwnerName(invitedEvent).subscribe(response =>  {        
          this.inviteOwnerName = response['FirstName'] + " " + response['LastName'];        

          this.checkConflicts(invitedEvent,this.allEvents);
      
          this.modalIsOpened = true;
          this.modalRef = this.modal.open(this.modalContent,{ size: 'sm'});          
        }
      );    
            
    }
  }

  checkConflicts(invitedEvent: CalendarEvent, allEvents: CalendarEvent[] ): boolean {    
    for (let i = 0; i < allEvents.length; i++) {
      if (invitedEvent.id != allEvents[i].id) {        
        if (DateComparison.isInConflict(invitedEvent,allEvents[i])) {
          let conflictedEvent: CalendarEvent = allEvents[i];        
          this.errorMessage = `This event conflicts with: \"${conflictedEvent.title}\" (${moment(conflictedEvent.start).format('DD/MM/YYYY hh:mm a')} - ${moment(conflictedEvent.end).format('hh:mm a DD/MM/YYYY')})`;  
          this.showErrorDiv = true;                  
          return true;
        }
      }
    }
    return false;
  }


  acceptInvite() {
    const request = this.invitesService.acceptInvite(this.invitedEvent);
    this.finishRequest(request);
  }

  
  finishRequest(request: Observable<any>) {
    request.subscribe((message) => {
      if (message['error'] != null) {
        this.errorMessage = message['error'];
        this.showErrorDiv = true;
      } else {
        this.refreshCalendarTrigger.emit();
        this.invitesService.refreshInvitesCountTrigger.next();
        this.modalRef.close();  
        this.modalIsOpened = false;        
      }             
    });
  
  }

  deleteInvite() {
    const request = this.invitesService.deleteInvite(this.invitedEvent);
    this.finishRequest(request);
  }

  public closeModal(): void {    
    this.modalIsOpened = false;     
    this.modalRef.close();    
  }

}
