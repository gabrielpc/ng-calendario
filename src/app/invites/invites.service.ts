import { BackendEventObject } from './../model/backend-event-object';
import { BackendInviteObject } from './../model/backend-invite-object';
import { EndpointService } from './../endpoint.service';
import { CalendarEvent } from 'angular-calendar';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {startOfDay,endOfDay,subDays,addDays,endOfMonth,isSameDay,isSameMonth,addHours,addMinutes} from 'date-fns';
import { colors} from "../utils/colors"
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class InvitesService {

  eventsComunicator : Subject<any> = new Subject<any>();  
  refreshInvitesCountTrigger : Subject<any> = new Subject<any>();
  openInvitesAcceptModalTrigger : Subject<any> = new Subject<any>();
  constructor(private http: HttpClient,
  private endpointService: EndpointService) { }



  getInvites(): Observable<any> {
    return this.http.get<BackendEventObject[]>(this.endpointService.getUrl("getInvites")).map(
      receivedEvents => receivedEvents.map(element => ({
            id: element.id,
            start: new Date(element.start),
            end: (element.end == null? undefined : new Date(element.end)),
            title: element.title,
            color: {
              primary: element.primaryColor,
              secondary: element.secondaryColor
            },            
            dateIsValid: true,
            isTheOwner: false           
          }))
    );        
  }
 
  getOwnerName(invitedEvent: CalendarEvent) : Observable<any> {
    return this.http.post(this.endpointService.getUrl('getOwnerName'),{id: invitedEvent.id});
  }

  acceptInvite(invitedEvent: CalendarEvent): Observable<any>{
    return this.http.post(this.endpointService.getUrl('acceptInvite'),{id: invitedEvent.id});
  }

  deleteInvite(invitedEvent: CalendarEvent): Observable<any>{
    return this.http.post(this.endpointService.getUrl('deleteInvite'),{id: invitedEvent.id});
  } 

  alreadyInvited(invitedUser: String, eventID: number): Observable<any>{
    return this.http.post(this.endpointService.getUrl('alreadyInvited'),{username: invitedUser, eventID: eventID});
  }
}