import { Invitation } from '../../model/invitation';
import { CalendarEvent } from 'angular-calendar';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import { Component, OnInit, ViewChild } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal';
import { CalendarioService } from '../../calendario/calendario.service';
import { InvitesService } from '../invites.service';
import { InvitedUserField } from '../../model/invited-user-field';
import { RegisterService } from '../../register/register.service';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'invite-modal-component',
  templateUrl: './invite-modal.template.html',
  styleUrls: ['./invite-modal.css']
})



export class InviteModalComponent implements OnInit {

  @ViewChild('modalContent') modalContent: TemplateRef<any>;  
  public modalRef: NgbModalRef;
  public eventToInvite: CalendarEvent;  
  public invitedUsers: Array<InvitedUserField> = new Array<InvitedUserField>();
  public latestInvitedUser: InvitedUserField = new InvitedUserField();
  public showSuccessDiv: boolean = false;

  constructor(private modal: NgbModal,
              private calendarioService: CalendarioService,
              private invitesService: InvitesService,
              private registerService: RegisterService,
              private authService: AuthService) { }

  ngOnInit() {
  }

  public openModal(eventToInvite: CalendarEvent): void {
    this.eventToInvite = eventToInvite;
    this.invitedUsers = [];
    this.latestInvitedUser.username = "";
    this.latestInvitedUser.notFound = false;
    this.latestInvitedUser.alreadyInvited = false;
    this.latestInvitedUser.selfInvited = false;
    this.showSuccessDiv = false;
    this.modalRef = this.modal.open(this.modalContent,{ size: 'sm'});    
  }

  addInputField() {
    let invitedUser = new InvitedUserField();

    invitedUser.username = this.latestInvitedUser.username;
    invitedUser.notFound = this.latestInvitedUser.notFound;
    invitedUser.selfInvited = this.latestInvitedUser.selfInvited;
    invitedUser.alreadyInvited = this.latestInvitedUser.alreadyInvited;

    this.invitedUsers.push(invitedUser);  

    this.latestInvitedUser.username = ""; 
    this.latestInvitedUser.notFound = false;
    this.latestInvitedUser.alreadyInvited = false;
    this.latestInvitedUser.selfInvited = false;    
  }

  removeInputField() {    
    this.latestInvitedUser = this.invitedUsers.pop();    
  }


  submitModal(form) {
    let formIsValid = true;   
    
    let usersToInvite: string[] = [];
    this.invitedUsers.forEach((invitedUser) => {
      this. validateInvitedUser(invitedUser, this.eventToInvite.id);
      if (invitedUser.notFound  || invitedUser.selfInvited ||  invitedUser.alreadyInvited || invitedUser.username.length == 0) {
        formIsValid = false;
      } else {
        usersToInvite.push(invitedUser.username);
      }
      
    });

    if (this.latestInvitedUser.notFound || this.latestInvitedUser.selfInvited || this.latestInvitedUser.alreadyInvited || this.latestInvitedUser.username.length == 0) {
      formIsValid = false;
    } else {
      usersToInvite.push(this.latestInvitedUser.username);
    }
   

    if (formIsValid) {
      const invitation : Invitation = {
        eventID: this.eventToInvite.id, 
        usersToInvite: usersToInvite
      };

      this.calendarioService.inviteUsers(invitation).subscribe(response => {        
        if (response['message'] == "Ok") {
          this.showSuccessDiv = true;
        }
      });
    }    
  }

  public closeModal(): void {    
    this.modalRef.close();
  }

 

  public validateInvitedUser(invitedUser: InvitedUserField, eventID: number) {

    for (let i = 0; i < this.invitedUsers.length; i++) {
      this.invitedUsers[i].alreadyInvited = false;
    }
    this.latestInvitedUser.alreadyInvited = false;

    this.invitedUsers.push(this.latestInvitedUser);

    for (let i = 0; i < this.invitedUsers.length; i++) {
      for (let j = 0; j < this.invitedUsers.length; j++) {
        if (i != j && this.invitedUsers[i].username.length > 0 && this.invitedUsers[j].username.length > 0) {
          if (this.invitedUsers[i].username == this.invitedUsers[j].username) {
            this.invitedUsers[i].alreadyInvited = true;
            this.invitedUsers[j].alreadyInvited = true;
          }
        }        
      }
    }

    this.invitedUsers.pop();

      this.registerService.userExists(invitedUser.username).subscribe(response => {        
          invitedUser.notFound = !response['message'];
      });

      if (!invitedUser.alreadyInvited) {
        this.invitesService.alreadyInvited(invitedUser.username, eventID).subscribe(response => {        
          invitedUser.alreadyInvited = response['message'];
        });
      }

      this.authService.getLoggedInUsername().subscribe(response => {
        invitedUser.selfInvited = (response['username'] == invitedUser.username)
      });
  }
  



}
