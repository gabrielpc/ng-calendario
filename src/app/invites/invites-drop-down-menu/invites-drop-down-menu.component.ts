import { InvitesService } from './../invites.service';
import { CalendarEvent } from 'angular-calendar';
import { Component, OnInit, TemplateRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'invites-drop-down-menu',
  templateUrl: './invites-drop-down-menu.html',
  styleUrls: ['./invites-drop-down-menu.css']
})
export class InvitesDropDownMenuComponent implements OnInit {

  @Output() openInvitesAcceptModalEmitter = new EventEmitter;
  invites: CalendarEvent[] = [];

  constructor(private invitesService: InvitesService) { }

  refreshInvitesCount() {
    this.invitesService.getInvites().subscribe(response => {      
      this.invites = response;
    }
  );
  }

  ngOnInit() {
    this.refreshInvitesCount();
    
    this.invitesService.refreshInvitesCountTrigger.subscribe(() =>
      this.refreshInvitesCount()
    );    
  }

  requestOpenInvitesAcceptModal(invitedEvent: CalendarEvent) {       
      this.invitesService.openInvitesAcceptModalTrigger.next(invitedEvent);    
  }
}
