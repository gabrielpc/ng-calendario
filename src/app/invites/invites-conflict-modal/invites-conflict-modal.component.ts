import { AuthService } from './../../auth/auth.service';
import { Observable } from 'rxjs/Observable';

import { DateComparison } from './../../utils/dateComparison';
import { InvitesService } from './../invites.service';
import { Invitation } from '../../model/invitation';
import { CalendarEvent } from 'angular-calendar';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import { Component, OnInit, Input,Output, ViewChild, EventEmitter } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal';
import { CalendarioService } from '../../calendario/calendario.service';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { setInterval } from 'timers';

@Component({
  selector: 'invites-conflict-modal',
  templateUrl: './invites-conflict-modal.html',
  styleUrls: ['./invites-conflict-modal.css']
})
export class InvitesConflictModalComponent implements OnInit {

  @ViewChild('modalContent') modalContent: TemplateRef<any>;  
  @Input() allEvents: CalendarEvent[] = [];
  @Output() refreshCalendarTrigger: EventEmitter<any> = new EventEmitter<any>();

  public modalRef: NgbModalRef;
  invitedEvent: CalendarEvent;
  inviteOwnerName: String;
  modalIsOpened: boolean = false;
  public conflictedEvent1: CalendarEvent;
  public conflictedEvent2: CalendarEvent;

  
  errorMessage: string;
  showErrorDiv: boolean = false;
  

  constructor(private modal: NgbModal,
  private calendarioService: CalendarioService,
  private invitesService: InvitesService,
  private authService: AuthService) { }


  ngOnInit() {
    this.errorMessage = "An invited event date has changed. The following events are in conflict:"
        
    setInterval(() => {
      if (this.authService.isAuthenticated() && !this.modalIsOpened) {      
      this.allEvents.forEach((event) => {            
          if (!event.isTheOwner)       {
            if (this.checkConflicts(event,this.allEvents)) {              
              this.openModal(event);
            }
          }                    
        });
      }
      },3000);
  }

  public openModal(invitedEvent: CalendarEvent): void { 
    
    this.showErrorDiv = false;   
    this.invitedEvent = invitedEvent;    
    
    this.modalRef = this.modal.open(this.modalContent,{ size: 'sm'});    
    this.modalIsOpened = true;
  }

  checkConflicts(invitedEvent: CalendarEvent, allEvents: CalendarEvent[] ): boolean {
    for (let i = 0; i < allEvents.length; i++) {
      if (invitedEvent.id != allEvents[i].id) {        
        if (DateComparison.isInConflict(invitedEvent,allEvents[i])) {          
          this.conflictedEvent1 = invitedEvent;
          this.conflictedEvent2 = allEvents[i];
          return true;
        }
      }
    }
    return false;
  }


 deleteEvent(index: number) {
   let eventToDelete = index == 1? this.conflictedEvent1 : this.conflictedEvent2;
   this.calendarioService.deleteEvent(eventToDelete.id).subscribe(() => {
    this.refreshCalendarTrigger.emit();
     this.closeModal();
   })
 }

  public closeModal(): void {    
    this.modalRef.close();
    this.modalIsOpened = false;
  }

}
