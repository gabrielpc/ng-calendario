import { FormatedDatePipe } from './../pipes/formated-date.pipe';
import { MyCustomEventTitlePipe } from './../pipes/my-custom-event-title.pipe';
import { NgModule } from '@angular/core';
import { FieldControlErrorComponent } from './field-control-error/field-control-error.component';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [    
    CommonModule
  ],
  declarations: [
    FieldControlErrorComponent,
    MyCustomEventTitlePipe,
    FormatedDatePipe
  ],
  exports: [
    FieldControlErrorComponent, 
    MyCustomEventTitlePipe, 
    FormatedDatePipe
  ],
  providers: []
})

export class SharedModule {}
