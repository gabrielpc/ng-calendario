import { EndpointService } from './../endpoint.service';
import { environment } from './../../environments/environment.prod';
import { Router } from '@angular/router';
import { Injectable, EventEmitter } from '@angular/core';

import { User } from '../shared/user';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';


import decode from "jwt-decode"
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {

  private usuarioAutenticado: boolean = false;
  private changeNavBarSignal: Subject<any> = new Subject<any>();
  private loginChange: Subject<any> = new Subject<any>();

  mostrarMenuEmitter = new EventEmitter<boolean>();

  constructor(private http: HttpClient,
              private endpointService: EndpointService) { }


  public getToken(): string {
    return localStorage.getItem('access_token');
  }
  public isAuthenticated(): boolean {
    // get the token
    const token = this.getToken();
    // return a boolean reflecting 
    // whether or not the token is expired
    return tokenNotExpired('access_token',this.getToken());
  }

  

  authenticate(user: User) : any{
    let httpResponse =  this.http.post(this.endpointService.getUrl("login"), JSON.stringify({'username': user.username,'password': user.password}));

    httpResponse.subscribe(
      // Caso de sucesso, o post retornara um Object com valores, um deles eh o access_token
      (data) => {                        
        localStorage.setItem('access_token',data['access_token']);     // Armazena o token devolvido pelo servidor, no LocalStorage do browser, sob chave com nome 'access_token'                
        this.loginChange.next(null);                                       // Envia sinal para a pagina inicial para saber que devemos atualizar a barra de navegacao
        this.changeNavBarSignal.next();
      },
      // Caso de erro
      (err: HttpErrorResponse) => {
        this.loginChange.next(err)
      }
    );  
    return httpResponse;     
  }

  public getLoginChange(): Subject<any> {
    return this.loginChange;
  }

  public getChangeNavBarSignal(): Subject<any> {
    return this.changeNavBarSignal;
  }

  public logout() {    
    localStorage.clear();
    setTimeout(() => this.changeNavBarSignal.next(), 50)    
  }

  isLoggedout() {
    return (localStorage.getItem("access_token") == null);
  }

  getLoggedInUsername() : Observable<any> {
    return this.http.get(this.endpointService.getUrl("getLoggedInUsername"));
  }
}