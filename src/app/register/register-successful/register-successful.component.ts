import { RegisterService } from '../register.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register-successful',
  templateUrl: './register-successful.template.html',
  styleUrls: ['./register-successful.css']
})
export class RegisterSuccessfulComponent implements OnInit {
  
  constructor() { }

  ngOnInit() {

  }

}
