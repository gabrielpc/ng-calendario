import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';
import { Input, OnInit, Component } from '@angular/core';
import {
  getSeconds,
  getMinutes,
  getHours,
  getDate,
  getMonth,
  getYear,
  setSeconds,
  setMinutes,
  setHours,
  setDate,
  setMonth,
  setYear
} from 'date-fns';


@Component({
  selector: 'time-picker',
  templateUrl: 'time-picker.html',
  styles: [
    `

  `
  ],  
})
export class TimePickerComponent implements OnInit{  

  @Input() time: Date;
  public hours: string;
  public minutes: string;

  constructor() {
    
  }

  ngOnInit() {
    this.hours = moment(this.time).format('hh');
    this.minutes = moment(this.time).format('mm');
  }

}
