import { CalendarEvent } from 'calendar-utils';
import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';

@Pipe({
  name: 'FormatedDate',
  pure: true
})

/** Formata o evento no formato: hh:mm a - Titulo 
 *  Caso o dia de inicio do evento seja ants do dia informado em viewDate (o dia que foi selecionado)
 *  o pipe outputa: 12:00am - Titulo
*/
export class FormatedDatePipe implements PipeTransform {

  transform(eventDate: Date): string {               
      let date = new DatePipe("en-US");      
      return moment(eventDate).format('DD/MM/YYYY (hh:mm a)');        
  }

}
