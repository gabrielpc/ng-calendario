import { SharedModule } from './../shared/shared.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CalendarModule } from 'angular-calendar';
import { UtilsModule } from '../utils/utils.module';

import { CalendarioComponent } from './calendario.component';
import { CalendarioService } from './calendario.service';

import { HeaderComponent } from './header/header.component';
import { EditModalComponent } from './edit-modal/edit-modal.component';
import { CustomCellComponent } from './custom-cell/custom-cell.component';
import { CustomEventTitleComponent } from './custom-event-title/custom-event-title.component';
import { FieldControlErrorComponent } from '../shared/field-control-error/field-control-error.component';
import { InviteModalComponent } from '../invites/invite-modal/invite-modal.component';
import { TimePickerComponent } from '../utils/time-picker';
import { InvitesAcceptModalComponent } from '../invites/invites-accept-modal/invites-accept-modal.component';
import { InvitesConflictModalComponent } from '../invites/invites-conflict-modal/invites-conflict-modal.component';

@NgModule({
  imports: [
    CommonModule,
    CalendarModule.forRoot(),
    UtilsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild([{ path: '', component: CalendarioComponent }])
  ],
  declarations: [
    HeaderComponent,
    CalendarioComponent,
    EditModalComponent,
    CustomCellComponent,
    CustomEventTitleComponent,
    InviteModalComponent,
    TimePickerComponent,
    InvitesAcceptModalComponent,
    InvitesConflictModalComponent
  ],
  exports: [CalendarioComponent],
  providers: [CalendarioService]
})

export class CalendarioModule {}
