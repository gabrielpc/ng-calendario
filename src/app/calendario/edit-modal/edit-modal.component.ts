import { DateComparison } from './../../utils/dateComparison';
import { EventsValidator } from './../../utils/events-validator';
import { AuthService } from './../../auth/auth.service';
import { colors } from '../../utils/colors';
import { CalendarEvent } from 'angular-calendar';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal';
import { startOfDay, endOfDay } from 'date-fns';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import { CalendarioService } from '../calendario.service';
import {MomentModule} from 'angular2-moment/moment.module';
import * as moment from 'moment';

@Component({
  selector: 'edit-modal-component',
  templateUrl: './edit-modal.template.html',
  styleUrls: ['./edit-modal.css']
})
export class EditModalComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;    
  @Output() refreshEmitter = new EventEmitter;
  @Output() addEventEmitter = new EventEmitter;
  @Output() updateEventEmitter = new EventEmitter;
  @Output() deleteEventEmitter = new EventEmitter;
  @Output() openInviteModalEmitter = new EventEmitter;

  @Input() allEvents: CalendarEvent[];
  @Input() viewDate: Date;

  public newEvents: CalendarEvent[] = [];
  public eventsToDisplay : CalendarEvent[];
  public originalEventsToDisplay : CalendarEvent[];
  public showNewEventMenu: boolean = false;
  public modalRef: NgbModalRef;
  public showErrorDiv: boolean = false;
  public errorMessage: string;  
  public addComplete: boolean = false;
  public updateComplete: boolean = false;
  

  constructor(private modal: NgbModal,
  private calendarioService: CalendarioService,
  private authService: AuthService) { }
  
  ngOnInit() {
    // Eventos emitidos por calendario.component
    this.calendarioService.modalCommunicator.subscribe(message => {      
      if (message['error'] != null) {                
        this.errorMessage = message['error'];
        this.showErrorDiv = true;
      }
      
      if (message['restore'] != null) {
        this.eventsToDisplay = this.originalEventsToDisplay;
      }
      if (message['closeModal'] != null) {
        this.closeModal();
      }
      if (message['delete'] != null) {        
        this.eventsToDisplay.splice(this.eventsToDisplay.indexOf(message['delete'],1));
        this.validateEvents(this.allEvents);
      }
      if (message['completed'] != null) {
        if (message['completed'] == "add") {
          this.addComplete = true;
        }
        if (message['completed'] == "update") {
         
          this.updateComplete = true;
          
          if (this.validateEvents(this.allEvents)){            
            this.closeModal();
          }                
        }        
      }
      if (this.addComplete && this.updateComplete) {
        this.closeModal();
      }
    })

    this.calendarioService.dateChanged.subscribe(changedDateEvent => {
      this.validateEvents(this.allEvents);
    })
  }

 
  validateEvents(events: CalendarEvent[]) : boolean {
  
    this.errorMessage = ""; 
    this.showErrorDiv = false; 

    let isValid: boolean = true;

    for (let i = 0; i < events.length; i++) {
      events[i].dateIsValid = true;  
    }
    for (let i = 0; i < this.newEvents.length; i++) {
      this.newEvents[i].dateIsValid = true; 
      events.push(this.newEvents[i]);
    }
    

    for (let i = 0; i < events.length; i++) {
      if (events[i].end.getTime() < events[i].start.getTime()) {
        events[i].dateIsValid = false;
        this.errorMessage = "The end date cannot be before the start date"
        this.showErrorDiv = true;
        isValid = false;
      }
      for (let j = 0; j < events.length; j++) {
        if (i != j) {
          if (DateComparison.isInConflict(events[i],events[j])) {              
            events[i].dateIsValid = false;                         
            events[j].dateIsValid = false;                         
            if (this.showErrorDiv == false) {
              this.errorMessage = `Conflicting events: \"${events[i].title}\" , \"${events[j].title}\"`;  
              this.showErrorDiv = true;                  
              isValid = false;
            }                
          }
        }      
      }
      if (events[i].title == undefined || events[i].title.length == 0) {
        isValid = false;
      }
    }
    
    for (let i = 0; i < this.newEvents.length; i++) {      
      events.pop();
    }
    return isValid;
  }

  // Abre o modal e edicai de eventos, exibindo os eventos daquele dia, fornecido como parametro pelo calendar.component, no metodo dayClicked
  public openModal(eventsToDisplay: CalendarEvent[]): void {
    this.newEvents = [];
    this.eventsToDisplay = eventsToDisplay;
    this.showErrorDiv = false;    
    this.addComplete = false;
    this.updateComplete = false;
    this.originalEventsToDisplay = JSON.parse(JSON.stringify(this.eventsToDisplay));

    this.validateEvents(this.allEvents);

  

    this.modalRef = this.modal.open(this.modalContent,{ size: 'lg'});
  }



  public submitModal(): void { 

    if (this.validateEvents(this.allEvents)) {
      // Se tiver pelo menos um elemento no array de novos eventos
      if (this.newEvents != undefined && this.newEvents.length > 0) {
        this.addEventEmitter.emit(this.newEvents);
      } else {
        this.addComplete = true;
      }
              
      let eventsToUpdate: CalendarEvent[] = [];
        
      for (let i = 0; i < this.eventsToDisplay.length; i++) {      
        if (JSON.stringify(this.eventsToDisplay[i]) != JSON.stringify(this.originalEventsToDisplay[i])) { 
          if (this.eventsToDisplay[i].dateIsValid) {
            eventsToUpdate.push(Object.assign({}, this.eventsToDisplay[i]));
          }
        }
      }

      if (eventsToUpdate != undefined && eventsToUpdate.length > 0) { 
          this.updateEventEmitter.emit(eventsToUpdate);
      }  else {
        this.updateComplete = true;
      }
      
      if (this.addComplete && this.updateComplete) {      
          if (!this.showErrorDiv) {
            this.closeModal();
          }  
      }
    }
  }

  deleteEventOnModal(event: CalendarEvent) {
    //if (confirm("Are you sure you want to delete this event?")) {
      this.deleteEventEmitter.emit(event);            
    //}
  }

  // Adiciona um novo evento fazio no array de novos eventos, soh pra aparece no modal
  public createNewEventField(): void {   
    // Cria um evento vazio
    let event: CalendarEvent = {   
      id: -1,  
      title: '',
      start: startOfDay(this.viewDate), //Seta a data inicial pra o inicio do dia que o usuario selecionou ao clicar
      end: endOfDay(this.viewDate),     //Seta a data final pra o fim do dia que o usuario selecionou ao clicar
      color: colors.red,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      dateIsValid: true,
      isTheOwner: true
    };
    
    
    // Se for o primeiro novo evento a ser adicionado
    if (this.newEvents == undefined) {
      this.newEvents = [event]; // Inicialize o array, soh com esse evento
    } else {
      // Adiciona o novo evento no inicio do array
      this.newEvents.unshift(event);    
    }
    
    // Avisa o calendar.component para atuaizar os componentes    
   // this.refreshEmitter.emit();        
  }

  public closeModal(): void {    
    this.modalRef.close();
    this.refreshEmitter.emit(); 
  }


  public removeNewEventField(index): void {  
    this.newEvents.splice(index, 1);    
    //this.refreshEmitter.emit();  
  }

  requestOpenInviteModal(eventToInvite: CalendarEvent) {
    if (this.showErrorDiv == false) {
      this.openInviteModalEmitter.emit(eventToInvite);
    }
  }
}
