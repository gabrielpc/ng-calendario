import { element } from 'protractor';
import { Invitation } from './../model/invitation';
import { BackendEventObject } from './../model/backend-event-object';
import { EndpointService } from './../endpoint.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/';
import { Injectable } from '@angular/core';
import {startOfDay,endOfDay,subDays,addDays,endOfMonth,isSameDay,isSameMonth,addHours,addMinutes} from 'date-fns';
import {CalendarEvent,CalendarEventAction,CalendarEventTimesChangedEvent} from 'angular-calendar';
import { colors} from "../utils/colors"
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { EventEmitter } from 'events';


@Injectable()
export class CalendarioService {
  
  public dateChanged : Subject<CalendarEvent> = new Subject<CalendarEvent>();
  modalCommunicator: Subject<any> = new Subject();

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {

      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
  
      }
    }
  ];

  constructor(private http: HttpClient,
              private endpointService: EndpointService) { }
  
  getEvents(): Observable<any> {

    return this.http.get<BackendEventObject[]>(this.endpointService.getUrl("getEvents")).map(
      receivedEvents => receivedEvents.map(element => ({
            id: element.id,
            start: new Date(element.start),
            end: (element.end == null? undefined : new Date(element.end)),
            title: element.title,
            color: {
              primary: element.primaryColor,
              secondary: element.secondaryColor
            },
            actions: this.actions,
            dateIsValid: true,
            isTheOwner: element.isTheOwner           
          }))
    );    
  }

  

addEvents(events): Observable<any> {        
  
    events.forEach(element => {
      element.start = element.start.getTime();      
      if (element.end != undefined)
        element.end = element.end.getTime();      
    });        

    return this.http.post(this.endpointService.getUrl("addEvents"),JSON.stringify(events));    
  }

  updateEvents(eventsToUpdate) : Observable<any>  {

    eventsToUpdate.forEach(element => {
      element.start = element.start.getTime();      
      if (element.end != undefined)
      element.end = element.end.getTime();      
    })
    
    return this.http.post(this.endpointService.getUrl("updateEvents"),JSON.stringify(eventsToUpdate));  
  }

  deleteEvent(eventID: Number) : Observable<any> {
    return this.http.post(this.endpointService.getUrl("deleteEvent"),{id: eventID});
  }

  inviteUsers(invitation: Invitation) : Observable<any> {
    return this.http.post(this.endpointService.getUrl("inviteUsers"),invitation);
  }

}
