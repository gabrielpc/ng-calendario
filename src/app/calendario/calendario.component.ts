import { InvitesService } from './../invites/invites.service';
import { InviteModalComponent } from '../invites/invite-modal/invite-modal.component';
import { element } from 'protractor';
import { AuthService } from './../auth/auth.service';
import { EditModalComponent } from './edit-modal/edit-modal.component';
import {Component,ChangeDetectionStrategy,ViewChild,TemplateRef, Input, OnInit} from '@angular/core';
import {startOfDay,endOfDay,subDays,addDays,endOfMonth,isSameDay,isSameMonth,addHours} from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {CalendarEvent,CalendarEventAction,CalendarEventTimesChangedEvent} from 'angular-calendar';
import { CalendarioService } from './calendario.service';
import { colors} from "../utils/colors"

@Component({
  selector: 'calendario-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['calendario.css'],
  templateUrl: 'calendario.template.html'
})
export class CalendarioComponent implements OnInit {  
  events: CalendarEvent[];                    // Todos os eventos do calendario
  view: string = 'month';                     // O modo padrao de visualizacao, por padrao, ver o mes inteiro
  viewDate: Date = new Date();                // A data atual, a qual dessejamos exibir detalhes. Começa com o dia atual
  refresh: Subject<any> = new Subject();      // Observer usado para indicar que eh necessario recarregar os componentes  
  activeDayIsOpen: boolean = false;           // Indica se devemos ou nao, abrir a barra preta com os eventos do dia  
  customCellTemplate: TemplateRef<any>;       // Variavel que faz referencia ao template definido em custom-cell/custom-cell.template.html
  customEventTitleTemplate: TemplateRef<any>; // Variavel que faz referencia ao template definido em custom-event-title/custom-event-title.template.html  

  constructor(
    private calendarioService: CalendarioService,
    private invitesService: InvitesService,
    private authService: AuthService) {}

  refreshCalendar() {
    if (this.authService.isAuthenticated()) {
      
      this.calendarioService.getEvents().subscribe(
        returnedEvents =>  { 
          this.events = returnedEvents;  
          this.invitesService.eventsComunicator.next(returnedEvents);
          this.refresh.next();
      });   

    }  else {
      console.log("Not logged in");
    }
  }
  

  ngOnInit() {
    this.events = [];   
    this.refreshCalendar();          
  }
  
  /*************************************************************************************************************************************************************
   * Metodo: dayClicked                                                                                                                                        *
   * Descricao : Faz o modal de edicao de eventos aparecer, quando o usuario clica em um dia                                                                   *
   * Argumentos: {date, events} = Eh um objeto do tipo 'day' que tem .date que te da a data completa do evento, e o .event que te retorna uma lista de eventos *
   * Retorno   : nenhum                                                                                                                                        *
   * ***********************************************************************************************************************************************************/
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }, editModalRef: EditModalComponent): void {
    // Se por acaso o usuario clicar em um dia fora do mes atual
    if (!isSameMonth(date, this.viewDate) && !isSameDay(this.viewDate, date)) {     
      this.viewDate = date; // Mudar o calendario para esse mes
    }

    // Abrir o modal de edicao de eventos para o dia clicado 
    editModalRef.openModal(events);

  }

  openInviteModal(event: CalendarEvent, inviteModalRef: InviteModalComponent) {    
    inviteModalRef.openModal(event);
  }

  /*************************************************************************************************************************************************************
   * Metodo: onMouseOverResponse                                                                                                                                       *
   * Descricao : Faz uma barra preta aparecer em baixo do evento que o usuario passa o mouse em cima, caso haja algum evento aquele dia                        *
   * Argumentos: {date, events} = Eh um objeto do tipo 'day' que tem .date que te da a data completa do evento, e o .event que te retorna uma lista de eventos *
   * Retorno   : nenhum                                                                                                                                        *
   * ***********************************************************************************************************************************************************/
  onMouseOverResponse({ date, events }: { date: Date; events: CalendarEvent[] }): void {              

    // Se o dia apontado eh diferente do que estavamos mostrando anteriormente, mas ainda eh no mesmo mes       
    if (!isSameDay(this.viewDate, date) && isSameMonth(date, this.viewDate)) {  
      this.viewDate = date; // Atualiza a data a ser mostrada      
          
      if (events.length === 0) {            // Se nesse dia nao tiver nenhum evento
        this.activeDayIsOpen = false;       // Nao mostrar a barrinha preta                
      } 
      // Caso tenha pelo menos um evento no dia selecionado
      else {                
          this.activeDayIsOpen = true;          // Abrir a barra preta                                                                          
      }             
    }  
  }  

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event, null);
    this.refresh.next();
  }

  // Utilizado quando se clica diretamente sobre um unico evento
  handleEvent(action: string, eventToDisplayOnModal: CalendarEvent, editModalRef: EditModalComponent): void {     
    editModalRef.openModal([eventToDisplayOnModal]);
  }
  
  // Adiciona novos eventos, enviados pelo edit-modal component, no array newEvents
  addEvents(newEvents: CalendarEvent[]): void {
    //  ADD AVENTS
  
      // Se nao tiver logado 
      if (!this.authService.isAuthenticated()) {    
        newEvents.forEach((element) => {
          this.events.push(element);
        });
        this.calendarioService.modalCommunicator.next({"completed": "add" }); 
      }       
      // If is logged in
      //  ADD AVENTS to backend
      else { 
        this.calendarioService.addEvents(newEvents).subscribe(response => {          
          if (response['message'] == "OK") {   
           /* newEvents.forEach((element) => {
              this.events.push(element);
            });*/
            this.calendarioService.modalCommunicator.next({"completed": "add" }); 
            this.refreshCalendar();  
          } else {          
            this.calendarioService.modalCommunicator.next({"error": response['message'] });              
          }         
        });
      }    
    
  }


  updateEvents(eventsToUpdate: CalendarEvent[]) {
     //  UPDATE EVENTS from backend
   if (this.authService.isAuthenticated()) {            
        this.calendarioService.updateEvents(eventsToUpdate).subscribe(response => {
          // Se der alguma coisa errada
          if (response['message'] != "OK") {
            this.calendarioService.modalCommunicator.next({"restore": true});
            this.calendarioService.modalCommunicator.next({"error": response['message']});              
          } else {
            this.calendarioService.modalCommunicator.next({"completed": "update", "closeModal": true });                       
            this.refreshCalendar();  
          }
        });         
    }  else {      
      this.calendarioService.modalCommunicator.next({"closeModal": true});
    }        
  }           

  public deleteEvent(event): void {    
    if (this.authService.isAuthenticated()) {
        this.calendarioService.deleteEvent(event.id).subscribe(response => {
            if (response['message'] == 'OK') {
             // this.events.splice(this.events.indexOf(event), 1);  
//              this.calendarioService.modalCommunicator.next({"delete": event});
              this.refreshCalendar();
            } else {
              this.calendarioService.modalCommunicator.next({"error": response['message']});              
            }
        });
    } else {       
      this.events.splice(this.events.indexOf(event), 1);          
      this.calendarioService.modalCommunicator.next({"delete": event});
    }    
    this.refresh.next();
  }
  

  assignCustomCellTemplate (cellTemplate) {
    this.customCellTemplate = cellTemplate;
  }

  assignCustomEventTitle (eventTitleTemplate) {    
    this.customEventTitleTemplate = eventTitleTemplate;    
  }

  refreshComponents() {
    this.refreshCalendar();
     
  }
}