import { Component, OnInit, ViewChild,Input, Output, EventEmitter, TemplateRef } from '@angular/core';

@Component({
  selector: 'custom-event-title',
  templateUrl: './custom-event-title.template.html',
  styleUrls: ['./custom-event-title.css']
})
export class CustomEventTitleComponent implements OnInit {
  @ViewChild('templateVar') templateVar: TemplateRef<any>;  // Referencia a variavel ng-template no .html desse componente, sera emitido para o componente principal, usando o templateVarEmitter  
  @Output()templateVarEmitter = new EventEmitter();
  @Input() viewDate: Date;
  constructor() { }

  ngOnInit() {
    this.templateVarEmitter.emit(this.templateVar);
  }

}
