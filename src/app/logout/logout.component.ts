import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.template.html',
  styleUrls: ['./logout.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router,
  private authService: AuthService) { 

  }

  ngOnInit() {
    this.authService.logout();
    setTimeout(() => {
      if (this.router.url == '/logout') {
        this.router.navigate(['/']);
      }
    }
    ,3000);
  }
 
}
