import { InviteModalComponent } from './invites/invite-modal/invite-modal.component';
import { CalendarEvent } from 'angular-calendar';
import { AuthService } from './auth/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy{
  
  public userIsLoggedIn : boolean = false;
  public loginChangeSubscription: Subscription;

  constructor(private authService: AuthService) {

  }

  ngOnInit() {    
    this.userIsLoggedIn =  this.authService.isAuthenticated();  

    this.loginChangeSubscription  = this.authService.getChangeNavBarSignal().subscribe(
      () => {      
        this.userIsLoggedIn =  this.authService.isAuthenticated()        
      }
    );
  }

  ngOnDestroy() {
    this.loginChangeSubscription.unsubscribe();
  }

  openInvitesAcceptModal(invitedEvent: CalendarEvent, modalRef: InviteModalComponent) {
    modalRef.openModal(invitedEvent);
  }
  
  
}
